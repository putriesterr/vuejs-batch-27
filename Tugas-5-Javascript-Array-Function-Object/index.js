//SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
for (var i = 0; i < daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}

console.log();
//SOAL2
function introduce(data){
    var nama = data.name;
    var umur = data.age;
    var alamat = data.address;
    var hobi = data.hobby;
    return("Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi)
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

console.log();
//SOAL 3
function hitung_huruf_vokal(str){
    const jumlahvokal = str.match(/[aeiou]/gi).length;
    return jumlahvokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


console.log();
// SOAL 4
function hitung(n) {
    var a = (2*n)
    var b = a-2
    return b
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8