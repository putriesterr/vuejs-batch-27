// SOAL 1
var nilai = 80;

if (nilai >= 85){
    console.log('A');
}else if ((nilai >= 75) && (nilai < 85)) {
    console.log('B');
}else if ((nilai >= 65) && (nilai < 75)) {
    console.log('C');
}else if ((nilai >= 55) && (nilai < 65)) {
    console.log('D');
}else if (nilai < 55) {
    console.log('E');
}
console.log();
   
//SOAL2
var tanggal = 27;
var bulan = 7;
var tahun = 2000;

switch (bulan){
    case 1 :
        console.log(tanggal + " Januari " + tahun);
        break;
    case 2 :
        console.log(tanggal + " Februari " + tahun);
        break;
    case 3 :
        console.log(tanggal + " Maret " + tahun);
        break;
    case 4 :
        console.log(tanggal + " April " + tahun);
        break;
    case 5 :
        console.log(tanggal + " Mei " + tahun);
        break;
    case 6 :
        console.log(tanggal + " Juni " + tahun);
        break;
    case 7 :
        console.log(tanggal + " Juli " + tahun);
        break;
    case 8 :
        console.log(tanggal + " Agustus " + tahun);
        break;
    case 9 :
        console.log(tanggal + " September " + tahun)
        break;
    case 10 :
        console.log(tanggal + " Oktober " + tahun);
        break;
    case 11 :
        console.log(tanggal + " November " + tahun);
        break;
    case 12 :
        console.log(tanggal + " Desember " );
        break;
    default :
        console.log("Eror");
}
console.log();

//SOAL 3
var  n = 3;
var p = '';
for (var i = 0; i < n;i++){
    for (var j = 0; j <= i; j++){
        p += '#';
    }
    p +='\n';
}
console.log(p);

console.log();

var  a = 7;
var s = '';
for (var i = 0; i < a;i++){
    for (var j =0; j <= i; j++){
        s += '#';
    }
    s +='\n';
}
console.log(s);

//SOAL 4
var m = 10;
var sd = '';
for (var i = 1; i <= m; i+=3){
    console.log(i + " - I love programming");
    var j =i+1;
    var l = j+1;
    sd += '=';
    while (j<l && j<=m){
        console.log(j+ " - I love Javascript");
        var k = j+1;
        var o = k+1;
        sd += '=';
        while ( k<o && k<=m){
            console.log(k+" - I love VueJs");
            sd += '=';
            k+= 3;
        }
    j+=3;
    console.log(sd);
    }
}
