//SOAL 1
luas = (a,b) => {
    let lebar = b
    let panjang = a
    return  panjang*lebar
}

keliling = (a,b) => {
    let lebar = a
    let panjang =b
    return 2*(panjang+lebar)
} 

console.log(luas(2,5));
console.log(keliling(2,5));

//SOAL 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code  
  newFunction("William", "Imoh").fullName() 

const NameString =  'Putri Ester'
// const will = `${NameString}`
const will = {NameString}
console.log(will)

//SOAL 3 
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  // Driver code
const {firstName,lastName,address,hobby} = newObject
console.log(firstName, lastName, address, hobby)

//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
let combinedarray = [...west,...east]
console.log(combined)
console.log(combinedarray);

//SOAL 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const str = `${before}`
console.log(str);