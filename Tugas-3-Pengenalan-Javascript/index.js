//SOAL 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var upper  = kedua.toUpperCase();

console.log(pertama[0]+pertama[1]+pertama[2]+pertama[3], 
    pertama[12]+pertama[13]+pertama[14]+pertama[15]+pertama[16]+pertama[17],
    kedua[0]+kedua[1]+kedua[2]+kedua[3]+kedua[4]+kedua[5]+kedua[6],
    upper[8]+upper[9]+upper[10]+upper[11]+upper[12]+upper[13]+upper[14]+upper[15]+upper[16]+upper[17])

//SOAL 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var number1 = Number(kataPertama);
var number2 = Number(kataKedua);
var number3 = Number(kataKetiga);
var number4 = Number(kataKeempat);
console.log((number1-number3+number4)*number2)

//SOAL 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); ; // do your own! 
var kataKetiga = kalimat.substring(15, 18); ; // do your own! 
var kataKeempat = kalimat.substring(19, 24); ; // do your own! 
var kataKelima = kalimat.substring(25, 31); ; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
