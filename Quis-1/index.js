//SOAL 1
function next_date(tanggal, bulan, tahun) {
    // if (tanggal ==31){
    //     tanggal =1;
    //     if (bulan==12){
    //         bulan=1;
    //         tahun+=1;
    //     } else{
    //         bulan+=1;
    //     }
    // }else {
    //     tanggal +=1;
    // }
    // switch (bulan){
    //     case 1 :
    //         console.log(tanggal + " Januari " + tahun);
    //         break;
    //     case 2 :
    //         console.log(tanggal + " Februari " + tahun);
    //         break;
    //     case 3 :
    //         console.log(tanggal + " Maret " + tahun);
    //         break;
    //     case 4 :
    //         console.log(tanggal + " April " + tahun);
    //         break;
    //     case 5 :
    //         console.log(tanggal + " Mei " + tahun);
    //         break;
    //     case 6 :
    //         console.log(tanggal + " Juni " + tahun);
    //         break;
    //     case 7 :
    //         console.log(tanggal + " Juli " + tahun);
    //         break;
    //     case 8 :
    //         console.log(tanggal + " Agustus " + tahun);
    //         break;
    //     case 9 :
    //         console.log(tanggal + " September " + tahun)
    //         break;
    //     case 10 :
    //         console.log(tanggal + " Oktober " + tahun);
    //         break;
    //     case 11 :
    //         console.log(tanggal + " November " + tahun);
    //         break;
    //     case 12 :
    //         console.log(tanggal + " Desember " );
    //         break;
    //     default :
    //         console.log("Eror");
    var string = tahun.toString() + "-" + bulan.toString() + "-" + tanggal.toString();
    var date = new Date(string);
    var nextDay = new Date(date);
    nextDay.setDate(date.getDate() + 1);
    var formatted_output = nextDay.toLocaleString('id-ID', {year: "numeric", month: "long", day: "numeric" });
    return formatted_output;
}
var t = 29;
var b = 2;
var th = 2020;

console.log(next_date(t , b , th ));
 // output : 1 Maret 2020

//SOAL 2
function jumlah_kata(str) {
    var jumlahkata = 1;
    var i = 0;
    for (var i = 0; i < str.length; i++){
        if(str.substring(i,i+1) === " "){
            jumlahkata+= 1;
        }
    }
    return jumlahkata;
}

var kalimat_1 = jumlah_kata(" Halo nama saya Muhammad Iqbal Mubarok ");
var kalimat_2 = jumlah_kata("Saya Iqbal");

console.log(kalimat_1,kalimat_2);
